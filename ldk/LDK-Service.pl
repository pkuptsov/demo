package PerlSvc;
use Win32::Service;
use Win32::TieRegistry(Delimiter=>"/");
use DBI;
use DBD::ODBC;
use Win32::EventLog;
use Net::Telnet;
####
our $Name = "LDK";
our $DisplayName = "LDK Statistics grabber";
####
$|=1;
$regkey="LMachine/Software/LDKStat";
####
sub Startup {
    Win32::Service::GetServices("", \%tmp);
    %svc=reverse %tmp;
    @reg=split /,/,my $reg=$Registry->{$regkey}->GetValue("services");
    $pwd=$Registry->{$regkey}->GetValue("Pwd")||die $!;
    $srv=$Registry->{$regkey}->GetValue("Srv")||die $!;
    $uid=$Registry->{$regkey}->GetValue("Uid")||die $!;
    $db=$Registry->{$regkey}->GetValue("Db")||die $!;
    $ip=$Registry->{$regkey}->GetValue("IP")||die $!;
    $timeout=$Registry->{$regkey}->GetValue("Timeout")||die $!;
    while (ContinueRun()) {
        foreach (@reg){
            if (Win32::Service::GetStatus("",$_,\%{$_})){
                unless(${$_}{'CurrentState'} eq ${$_}{'Previous'}){
                    ${$_}{'Previous'}=${$_}{'CurrentState'};
                    $DSN="driver={SQL Server};Server=$srv;database=$db;uid=$uid;pwd=$pwd";
                    $dbh = DBI->connect("dbi:ODBC:$DSN", { PrintError=>0,RaiseError => 0});
                    my $sname=$svc{$_};
                    my $state=${$_}{'CurrentState'};
    if ($dbh){
        $exec=$dbh->prepare("INSERT INTO statistic (call_from,co_line,time,date,dialed,act,cnt,cost,acc_code) VALUES(?,?,?,?,?,?,?,?,?)");
        # 
        $obj = new Net::Telnet (
                         Host       => $ip,
                         Prompt     => '/ENTER PASSWORD\s*/i',
                         Timeout    => $timeout
                         );
        $line=$obj->getline();
        $line=$obj->getline();
        my $ok = $obj->put("\n\n");
        while($line=$obj->getline) {
            next if(length($line)<1);
            next if($line=~/^[-]+/);
            next unless($line=~m/^[0-9]+/);
            ($call_from,$co_line,$duration,$datetime,$dialed,$act,$cnt,$cost,$account_code)=getLineFromSession($line);
            $exec->execute($call_from,$co_line,$duration,$datetime,$dialed,$act,$cnt,$cost,$account_code) if(defined $call_from);
            }
        $exec->finish;
        } else {
        Win32::EventLog::WriteEventLog ($ENV{Computer}, "$DisplayName", "1", "0", "1", "\n$DBI::errstr\n", "\n\$! $!\n$DBI::errstr\n");
    }
}
} else {
        unless (${$_} eq "1"){
            ${$_}=1;
            $DSN="driver={SQL Server};Server=$srv;database=$db;uid=$uid;pwd=$pwd";
            $dbh = DBI->connect("dbi:ODBC:$DSN", { PrintError=>1,RaiseError => 1});
            if ($dbh){
                $exec=$dbh->prepare("INSERT INTO statistic (call_from,co_line,time,date,dialed,act,cnt,cost,acc_code) VALUES(?,?,?,?,?,?,?,?,?)");
                #
        $obj = new Net::Telnet (
                         Host       => $ip,
                         Prompt     => '/ENTER PASSWORD\s*/i',
                         Timeout    => $timeout
                         );
        $line=$obj->getline();
        $line=$obj->getline();
        my $ok = $obj->put("\n\n");
        while($line=$obj->getline) {          
            next if(length($line)<1);
            next if($line=~/^[-]+/);
            next unless($line=~m/^[0-9]+/);
            ($call_from,$co_line,$duration,$datetime,$dialed,$act,$cnt,$cost,$account_code)=getLineFromSession($line);
            $exec->execute($call_from,$co_line,$duration,$datetime,$dialed,$act,$cnt,$cost,$account_code) if(defined $call_from);
            }
        $exec->finish;
        } else {#write to eventlog $dbi::errstr
        Win32::EventLog::WriteEventLog ($ENV{Computer}, "$DisplayName", "1", "0", "1", "\n$DBI::errstr\n", "\n\$! $!\n$DBI::errstr\n");
        }
        }
    }
}

}# ����� ����� ContinueRun()
$dbh->disconnect;
exit 0;
}

#sub Pause {
#    exit 0;
#    }

sub Install {
    print "The $Name Service has been installed.\n";
    print "Start the service with the command: net start $Name\n";
    my $reg=$Registry->{$regkey}->GetValue("services")||die $!;
    print "\nWatching for...\n\t".(join ",",$reg);
}
sub Remove {
print "The $Name service has been removed.\n";
}

sub Help {
print "Add values in reg from file settings.reg\n";    
print "Run this program in command:\n LDK-Service.exe --install auto\n";
}

sub getLineFromSession {
        my $line=shift || return undef;
        my($no,$call_from,$co_line,$duration,$date,$time,$dialed,$act,$cnt,$cost,$account_code)=split(/\s+/,$line,10);
        if($co_line=~/^\d{2}:\d{2}:\d{2}$/g) {# ������ ������ ������ �����
        $co_line='INT';   
        ($no,$call_from,$duration,$date,$time,$dialed,$act,$cnt,$cost,$account_code)=split(/\s+/,$line,9);
	$duration=calcTime($duration);
        $date=~m/(\d{2})\/(\d{2})\/(\d{2})/g;
        my $datetime='20'.$3.'-'.$2.'-'.$1.' '.$time .':'.'00';
        return ($call_from,$co_line,$duration,$datetime,$dialed,$act,$cnt,$cost,$account_code) if($no=~m/^\d{4}/);
        } else {
	$duration=calcTime($duration);    
        $date=~m/(\d{2})\/(\d{2})\/(\d{2})/g;
        my $datetime='20'.$3.'-'.$2.'-'.$1.' '.$time .':'.'00';
        return  ($call_from,$co_line,$duration,$datetime,$dialed,$act,$cnt,$cost,$account_code) if($no=~m/^\d{4}/);
        }
    return undef;    
}

sub calcTime {
    my $duration=shift;
    my ($hour,$min,$sec)=(0,0,0);
    ($hour,$min,$sec)=split(/:/,$duration);
    $hour=~s/[^0-9]+//g;
    $min=~s/[^0-9]+//g;
    $sec=~s/[^0-9]+//g;
    return ($hour*60*60 + $min*60 + $sec);
}