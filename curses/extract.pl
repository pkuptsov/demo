#!/usr/bin/perl -w
#
##SELECT T0.RateDate, T0.Currency, T0.Rate FROM ORTT T0 WHERE T0.RateDate =N'2007-01-07'
# version 3.0 ��� ������ � ����������� �� �� MSSQL 2005
use strict;
use LWP::Simple;
use DBI;
use DBD::ODBC;

#############################################
my %setup=parse_config();
my $flag=undef;
my $str='';
my %hash=();
my $date=t_format();
my $url="http://www.cbr.ru/currency_base/D_print.asp?date_req=$date";
##############################################
my $content=striptohtml(get($url));

for my $line(split(/\n/,$content)) {
	$flag=1if ($line=~/^\<tr/);
	$flag=0 if ($line=~/^\<\/tr/);
	unless($flag) {
		if($str=~/\>840\</g) {
		$str=~/(\d+\D\d+)<\/td>/g;
		$hash{'USD'}=$1;
		}
		if($str=~/\>978\</g) {
		$str=~/(\d+\D\d+)<\/td>/g;
		$hash{'EUR'}=$1;
		}
		if($str=~/\>756\</g) {
		$str=~/(\d+\D\d+)<\/td>/g;
		$hash{'CHF'}=$1;
		}		
		$str=''; 
	}
	$str.=$line if($flag);
}

open(DEBUG,">>","curs.txt") || die "can't create file: $!\n";
print DEBUG $date,"\n";
print DEBUG "USD: $hash{USD}\n";
print DEBUG "EUR: $hash{EUR}\n";
print DEBUG "CHF: $hash{CHF}\n";
print DEBUG "==================\n";
#SELECT T0.RateDate, T0.Currency, T0.Rate FROM ORTT T0 WHERE T0.RateDate =N'2007-01-07'
### 

foreach my $kk(split (/,/,$setup{db})) {# ������ ������� �� ������ ��
# ���������� DSN
my $DSN="driver={SQL Server};Server=$setup{server};database=$kk;uid=$setup{user};pwd=$setup{pass}";
# ������������ � ���������� ��
my $dbh = DBI->connect("dbi:ODBC:$DSN", { PrintError=>1,RaiseError => 1}) or die "$DBI::errstr\n";
$dbh->{AutoCommit} = 0;# ��������� ����������
# ��������� ������ � ��
foreach my $key(keys %hash) {
	next if $key eq 'DATE';
	$hash{$key}=~s/,/./;
my $sql=qq(INSERT INTO ORTT VALUES(convert(datetime,'$hash{DATE}',120),N'$key',$hash{$key},'I',13));
my $sth=$dbh->do($sql);
if($sth) {
	$dbh->commit;# ��������
}
else {
	$dbh->rollback;# ����������
	print DEBUG "Error at ". localtime().": $sql\n$DBI::errstr\n";
	}
}
# ����������� �� ��
$dbh->disconnect;
} # ����� ������� �� ��
# ��������� ���� ������� � �������.
close(DEBUG);
exit;

## Subs
sub striptohtml
{
    ($_) = @_;
    s/\n//g;
    s/\t//g;
    s/\s\s+/ /g;
    s/\</\n\</sg;
    s/\>/\>\n/sg;
    return($_);
}

sub t_format {
my @time=localtime;
my $year = sprintf("20%02d", $time[5] % 100);
my $month=$time[4]+1;
$month=length($month)<2 ? "0$month" : $month;
my $day=length($time[3])<2 ? "0$time[3]" : $time[3];
$hash{DATE}="$year-$month-$day 00:00:00";
return qq($day/$month/$year);
}

sub parse_config {# ������ ���������������� ���� config.dat
my %CONFIG;
open(CFG,"<","./config.dat") || die "Config not found: $!\n";
while(<CFG>) {
next if(/^\#/);
chomp;
my($key,$value)=split(/=/,$_);
$CONFIG{$key}=$value if(defined($key) && defined($value)) ;
}
return %CONFIG;
}