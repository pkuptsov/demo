#!/usr/bin/perl
#
no warnings 'layer';
use strict;
use XML::Bare;

#
my $file = $ARGV[0];# || 'bankruptcy.xml';
chomp($file);
die "Error! File not found" unless ( -e $file );

### GLOBAL VARS ###
my $ob = XML::Bare->new( file => $file );
my $root = $ob->parse();

# массив для хранения данных с заявками
my $orders = $root->{feed}->{'f:orders'}->{'f:order'};

# массив для хранения данных о компаниях
my $companies = $root->{feed}->{'f:companies'}->{'f:company'};

# массив с типами заявок
my $orderType = $root->{feed}->{dictionaries}->{orderType};

# хэш компаний
my %company;

# типы заявок
my %ordertypes;

#
my $prefix = time();

open( OUT, ">", $file . "_" . $prefix . ".html" ) || die "$!";

### MAIN ####
# step 1
# формируем БД реквизитов компаний
print
  "Generate company database................................................";
foreach my $el (@$companies) {

    # поиск адресов
    # $addr[0] - юр.,1факт,2почт,3тел,4факс,5email
    my $array = $el->{'f:contact'};
    my $addr  = getAddr($array);

    # ключ хэша id компании
    $company{ $el->{id}->{value} } =
qq($el->{'f:name'}->{value}:$el->{inn}->{value}:$el->{kpp}->{value}:$el->{ogrn}->{value}:$addr->[0]:$addr->[1]:$addr->[2]:$addr->[3]:$addr->[4]:$addr->[5]);

}
print "OK\n";

# step 2
# формируем БД типов заявок
print
  "Generate order types database............................................";
foreach my $el (@$orderType) {
    $ordertypes{ $el->{id}->{value} } = $el->{name}->{value};
}
print "OK\n";

print OUT genHTMLstart();

# step 3
# обрабатываем заявки
print
  "Processing orders........................................................";
foreach my $el (@$orders) {
    $el->{modifiedDate}->{value} =~ /^(\d{4})-(\d{2})-(\d{2})/g;
    my $date_load = "$3.$2.$1";
    $el->{publDate}->{value} =~ /^(\d{4})-(\d{2})-(\d{2})/g;
    my $date_pub   = "$3.$2.$1";
    my $text_array = $el->{'f:maket'}->{body}->{p};
    my $text       = getAllText($text_array);

    # ID заявки
    my $string =
qq($el->{id}->{value}:$el->{'f:advertiser'}->{company}->{value}:$date_load:$el->{name}->{value}:$ordertypes{$el->{orderType}->{value}}:$date_pub:$company{$el->{'f:advertiser'}->{company}->{value}}:$text);
    print OUT genHTMLdetails($string);
}
print "OK\n";

print "Finish!\n";

print OUT genHTMLend();
close(OUT);

#### SUBS
sub getAllText {
    my $array_ref = shift;
    my $string;
    if ( ref($array_ref) eq 'ARRAY' ) {
        foreach my $el (@$array_ref) {
            $string .= $el->{text}->{value};
        }
    }
    else {
        $string .= $array_ref->{text}->{value};
    }
    $string =~ s/&lt;/</g;
    $string =~ s/&gt;/>/g;
    return $string;
}

sub getAddr {
    my $array_ref = shift;    # || return 0;
    my @array;
    if ( ref($array_ref) eq 'ARRAY' ) {
        foreach my $el (@$array_ref) {
            $array[0] = $el->{value} if ( $el->{type}->{value} eq 1 );
            $array[1] = $el->{value} if ( $el->{type}->{value} eq 2 );
            $array[2] = $el->{value} if ( $el->{type}->{value} eq 3 );
            $array[3] = $el->{value} if ( $el->{type}->{value} eq 4 );
            $array[4] = $el->{value} if ( $el->{type}->{value} eq 5 );
            $array[5] = $el->{value} if ( $el->{type}->{value} eq 6 );
        }
    }
    else {
        $array[0] = $array_ref->{value} if ( $array_ref->{type}->{value} eq 1 );
        $array[1] = $array_ref->{value} if ( $array_ref->{type}->{value} eq 2 );
        $array[2] = $array_ref->{value} if ( $array_ref->{type}->{value} eq 3 );
        $array[3] = $array_ref->{value} if ( $array_ref->{type}->{value} eq 4 );
        $array[4] = $array_ref->{value} if ( $array_ref->{type}->{value} eq 5 );
        $array[5] = $array_ref->{value} if ( $array_ref->{type}->{value} eq 6 );
    }
    return \@array;
}

sub genHTMLstart {
    return qq(
        <html>
        <head>
        <meta http-equiv=Content-Type content="text/html; charset=utf-8">
        </head>
        
        <body>
        <table border=0>
         <tr>
          <td style="white-space: nowrap">ID заявки</td>
          <td style="white-space: nowrap">ID банкрота</td>
          <td style="white-space: nowrap">Дата выгрузки</td>
          <td style="white-space: nowrap">Название заявки (по номеру счета)</td>
          <td style="white-space: nowrap">Тип заявки</td>
          <td style="white-space: nowrap">Дата публикации</td>
          <td style="white-space: nowrap">Название</td>
          <td style="white-space: nowrap">ИНН</td>
          <td style="white-space: nowrap">КПП</td>
          <td style="white-space: nowrap">ОГРН</td>
          <td style="white-space: nowrap">Адрес юридический</td>
          <td style="white-space: nowrap">Адрес физический</td>
          <td style="white-space: nowrap">Адрес почтовый</td>
          <td style="white-space: nowrap">Телефон(ы)</td>
          <td style="white-space: nowrap">Факс(ы)</td>
          <td style="white-space: nowrap">E-mail(ы)</td>
          <td style="white-space: nowrap">Комментарии</td>
         </tr>  
    )
}

sub genHTMLdetails {
    my $string = shift;
    my (
        $c1,  $c2,  $c3,  $c4,  $c5,  $c6,  $c7,  $c8, $c9,
        $c10, $c11, $c12, $c13, $c14, $c15, $c16, $c17
    ) = split( /:/, $string, 17 );
    return qq(
    <tr>
     <td>$c1</td>
          <td style="white-space: nowrap">$c2</td>
          <td style="white-space: nowrap">$c3</td>
          <td style="white-space: nowrap">$c4</td>
          <td style="white-space: nowrap">$c5</td>
          <td style="white-space: nowrap">$c6</td>
          <td style="white-space: nowrap">$c7</td>
          <td style="white-space: nowrap">$c8</td>
          <td style="white-space: nowrap">$c9</td>
          <td style="white-space: nowrap">$c10</td>
          <td style="white-space: nowrap">$c11</td>
          <td style="white-space: nowrap">$c12</td>
          <td style="white-space: nowrap">$c16</td>
          <td style="white-space: nowrap">$c14</td>
          <td style="white-space: nowrap">$c15</td>
          <td style="white-space: nowrap">$c13</td>
          <td style="white-space: nowrap">$c17</td>
         </tr>  
    );
}

sub genHTMLend {
    return qq(</table></body></html>);
}
